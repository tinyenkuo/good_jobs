import requests
from bs4 import BeautifulSoup
import re

import time
import csv
import sys
from sys import stdout
from datetime import datetime
from dateutil import parser
import json

# for Django Model
from crawler_starter.models import Job

should_finish = False
all_jobs = []

# ----------------------------------------------------
# Declare
# ----------------------------------------------------

def get_contents(content_href):
	res = requests.get(str(content_href))
	soup = BeautifulSoup(res.text, 'html.parser')
	content = soup.select(".bbs-screen.bbs-content")[0].get_text()#上一頁網址
	#print(content)
	# content  文章內文
	if soup.select('.article-meta-value'):
		try:  #內文 (date到"發信站"之間的文字)
			date = soup.select('.article-meta-value')[3].text
			full_content = soup.find(id="main-content").text
			target_content = u'※ 發信站: 批踢踢實業坊(ptt.cc),'
			cont = full_content.split(target_content)
			main_cont = cont[0].split(date)
			main_content = main_cont[1].replace('\n', '  ')
			#print('content:' + str(main_content))
			#print("content[1]: ")
			#print(content[1].split(u"公司名稱")[1])
		except Exception as e:
			main_content = 'main_content error'
			print('main_content error URL')
			print(content_href)
		# print("------------------------------------")
		return main_content
	else:
		main_content = "NO Date, no content!!"
		return main_content


def get_company_name_from_contents(contents):
	# [ 內容前處理 ]
	contents = contents.replace(' ', '')
	contents = contents.replace('工作地點', '公司地址')

	companyName = ""

	# [ 取得「公司地址」前的subString ]
	try:
		pat = '公司地址'
		match = re.search(pat,contents)
		endOfCompanyName = match.start()
		companyName = contents[:endOfCompanyName]
		print("[P1]")
	except: pass

	# [取得「公司名稱」後的subString]
	try:
		pat = '公司名稱'
		match = re.search(pat,companyName)
		startOfCompanyName = match.start()
		companyName = companyName[startOfCompanyName:]
		print("[P2]")
	except: pass

	print(companyName)

	# [ 過濾雜質 ]

	pat1 = ']'
	pat2 = '】'
	pat3 = '【'
	pat4 = '['
	pat5 = '公司名稱'

	companyName = companyName.replace(pat1,'')
	companyName = companyName.replace(pat2,'')
	companyName = companyName.replace(pat3,'')
	companyName = companyName.replace(pat4,'')
	companyName = companyName.replace(pat5,'')

	# [去除「:」前的部分]
	try:
		pat = ':'
		match = re.search(pat,companyName)
		startOfCompanyName = match.start()
		companyName = companyName[startOfCompanyName+1:]
		print("[P3]")
	except: pass

	# [去除「：」前的部分]
	try:
		pat = '：'
		match = re.search(pat,companyName)
		startOfCompanyName = match.start()
		companyName = companyName[startOfCompanyName+1:]
		print("[P4]")
	except: pass

	return companyName
	


def parse_jobs_with(res):

	global should_finish
	global all_jobs

	soup = BeautifulSoup(res.text, 'html.parser')

	jobs = soup.find_all('div','r-ent')
	for job in jobs:

		push_count = 0

		push_count_div = job.find('div', 'nrec')
		push_count_string = push_count_div.string

		if push_count_string:
			try:
				push_count = int(push_count_string)  # 轉換字串為數字
			except ValueError:  # 若轉換失敗，不做任何事，push_count 保持為 0
				pass

		#取得日期
		date = 0
		date_div = job.find('div','date')
		date_string = date_div.string 

		int_date = ""

		if date_string:

			try :
				int_date = parser.parse(date_string)

				if int_date.month == 12:
					should_finish = True
					break
			except:
				should_finish = True
				break


		#取得作者
		author_div = job.find('div','author')
		author_string = author_div.string 

		if author_div:
			author = author_string

		#取得文章連結及標題
		link_div = job.find('a')

		if link_div:
			href = link_div['href']
			title = link_div.string 

			href = "https://www.ptt.cc" + href

			content = get_contents(href)

			company_name = get_company_name_from_contents(content)
			print("result ->")
			print(company_name)

			# print(type(content))
			# print(company_name)

			# if title != None:
				# if "公告" in title:
					# print("這是公告RRR")

			insert_job = {
				'title': title,
				'date': str(int_date.date()),
				'author': author,
				'href': href,
				'push_count': push_count,
				'content': content
			}

			job = Job.objects.create(
                    title = title,
                    date = str(int_date.date()),
                    author = author,
                    href = href,
                    push_count = push_count,
                    content = content,
                    company_name = company_name
                )

			all_jobs.append(insert_job)

			# print(insert_job)


def next_page(page_index):
	url_prefix = "https://www.ptt.cc/bbs/Soft_Job/index"
	url_postfix = ".html"
	url = url_prefix + str(page_index) + url_postfix
	return url


def first_parse():
	url = "https://www.ptt.cc/bbs/Soft_Job/index.html"
	res = requests.get(url)
	parse_jobs_with(res)


# ----------------------------------------------------
# Main
# ----------------------------------------------------

def main():

	global should_finish

	page_index = 1277
	first_parse()

	while should_finish != True or page_index < 0:
		url = next_page(page_index)
		
		try:
			res = requests.get(url)
			parse_jobs_with(res)
		except requests.exceptions.RequestException as e:
			print("error -> ")
			print(e)
			continue

		page_index -= 1

		progress = 1 - (page_index/1273)
		# round_progress = round(progress, 0)
		percentage = progress * 100
		print("\r現在進行到" + str(percentage))
		# sys.stdout.flush()

	print(all_jobs)

def dropPTT():
	Job.objects.all().delete()

# ----------------------------------------------------
# Run
# ----------------------------------------------------
# main()