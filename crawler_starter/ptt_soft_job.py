# -*- coding: utf-8 -*- 
import requests
from bs4 import BeautifulSoup
import re

import time
import csv
import sys
from datetime import datetime
from dateutil import parser
import json

# from crawler_starter.models import Job 

def get_articles(res, articles):
    flag = 0
    soup = BeautifulSoup(res.text, 'html.parser')

    jobs = soup.find_all('div', 'r-ent')
    for job in jobs:
        #if d.find('div', 'date').string == date:  # 發文日期正確
        # 取得推文數
        push_count = 0

        push_count_div = job.find('div', 'nrec')
        push_count_string =  push_count_div.string

        if push_count_string:
            try:
                push_count = int(push_count_string)  # 轉換字串為數字
            except ValueError:  # 若轉換失敗，不做任何事，push_count 保持為 0
                pass

        date_div = job.find('div', 'date')
        date_string = date_div.string

        if date_string:
            int_date = parser.parse(date_string)
            if int_date.month==1:
                print("month yes") 
                if int_date.day==1:
                    print("day yes")
                    flag = 1
            print(int_date)

        author_div = job.find('div', 'author')
        author_string = author_div.string

        if author_div:
            author = author_string

        # 取得文章連結及標題 

        link_div = job.find('a')

        if link_div:  # 有超連結，表示文章存在，未被刪除
            href = link_div['href']
            title = link_div.string
            print(title)

            href = "https://www.ptt.cc" + href

            if title != None:
                if "公告" in title:
                    print("這是公告RRR")
                    flag = 0
            articles.append({
                'title': title,
                'date': str(int_date.date()),
                'author': author,
                'href': href,
                'push_count': push_count,
            })

            # job = Job.objects.create(
            #         title = title,
            #         date = str(int_date.date()),
            #         author = author,
            #         href = href,
            #         push_count = push_count,
            #     )

    return articles,flag

#-------------主程式---------------

def main():

    flag = 0
    articles = []  # 儲存取得的文章資料
    #browser = open_main_browser()
    url = "https://www.ptt.cc/bbs/Soft_Job/index.html"
    res = requests.get(url)

    page_index = 1273
    # f = open('SoftJob.json', 'w', encoding = 'utf-8') -> for json

    while flag != 1:
        articles,flag = get_articles(res,articles)
        url = "https://www.ptt.cc/bbs/Soft_Job/index"+str(page_index)+".html"
        print(url)
        # res = requests.get(url)
        
        try:
            res = requests.get(url)
        except requests.exceptions.RequestException as e:  # This is the correct syntax
            print(e)
            continue

        page_index -= 1

    # file = { 'Title' : articles } -> for json
    # f.write(json.dumps(file, ensure_ascii = False, indent = 4)) -> for json
    # f.close() -> for json
main()
