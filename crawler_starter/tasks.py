from celery.decorators import task 
import time
 
from good_jobs import celery_app

from .ptt import main
from .ptt import dropPTT

@celery_app.task
def sendmail(email):
    print('start send email to %s' % email)
    time.sleep(5) #休息5秒
    print('success')
    return True


from celery.task.schedules import crontab  
from celery.decorators import periodic_task
 
@periodic_task(run_every=crontab(minute=10, hour=21))
def runPTTCrawler():
	print("Start Run PTT!")
	dropPTT()
	main()
	return True

