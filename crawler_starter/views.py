from django.shortcuts import render
from django.http import HttpResponse
from .tasks import runPTTCrawler

from crawler_starter.models import Job 
from django.core import serializers

def index(request):
    # sendmail.delay('test@test.com')
    # runPTTCrawler()
    jobs = Job.objects.all()

    w_dict = {'jobs':jobs}

    return render(request, 'crawler_starter/index.html',context=w_dict)

    # jobs_json = serializers.serialize("json", jobs)
    # return HttpResponse(jobs_json)