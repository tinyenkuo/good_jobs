from django.apps import AppConfig


class CrawlerStarterConfig(AppConfig):
    name = 'crawler_starter'
