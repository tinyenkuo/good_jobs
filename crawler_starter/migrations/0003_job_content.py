# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-08-19 16:37
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('crawler_starter', '0002_auto_20170807_1527'),
    ]

    operations = [
        migrations.AddField(
            model_name='job',
            name='content',
            field=models.TextField(default='Content'),
        ),
    ]
