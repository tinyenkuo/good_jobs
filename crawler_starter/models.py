from django.db import models

class Job(models.Model):

	title = models.CharField(max_length=100)
	date = models.CharField(max_length=100)
	author = models.CharField(max_length=100)
	href = models.URLField(max_length=200)
	push_count = models.CharField(max_length=100)
	content = models.TextField(default='Unknown')
	company_name = models.CharField(max_length=100,default='Unknown')

	
